# GitLab Tutorial
This document is still a work in progress. It will be used as a tutorial to help explain the basics of Git and GitLab to users unfamiliar with them.

## Table of Contents

**[Git](#git)**

* [Command Line](#command-line)
* [GitHub Program](#github-program)
  * [Setup](#setup)
  * [Mac Instructions](#mac-instructions)
  * [Windows Intructions](#windows-instructions)
* [Best Practices](#best-practices)

**[GitLab](#gitlab)**
* [Markdown](#markdown)
* [Dashboard](#dashboard)
* [Project](#project)
* [Files](#files)
* [Commits](#commits)
* [Issues](#issues)
* [Merge Requests](#merge-requests)
* [Snippets](#snippets)

## Git
Git is a free and open-source distributed version control system. It is much faster than other alternatives such as subversion or cvs. Git performs almost all operations locally, which gives it a huge speed advantage over centralised systems that have to constantly communicate with a server somewhere.

Git is distributed, which means that even if you're using a centralised workflow, every user essentially has a full backup of the main server. Each of these copies could be pushed up to replace the main server in the event of a crash or corruption.

### Command Line

**Tutorial** - https://try.github.io/

This is a web based command line tutorial which allows you to type in the commands into a terminal to practice using Git. If you have never used git before this is a good place to start.

**Cheat Sheet** - http://rogerdudler.github.io/git-guide/files/git_cheat_sheet.pdf

There are several basic commands that are essential to use Git in the command line. See this pdf for a decent overview of some of the commands. This is useful to use as a reference for remembering the commands after completing the tutorial.

**Note:** If you have any uncomitted changes and perform `git pull` then you can overright your local changes. Look at the `git stash` command on how to save your local changes before syncing with server. This is only a problem in the command line, the GitHub desktop app's do not have this problem.

### GitHub Program

Git doesn't just work using the command line interface, it can also be used with an application such as the official GitHub application. This makes it very easy to commit changes to a repository without needing to remember any commands. Using GitLab with the GitHub application can take some extra setup with the command line, but after the initial setup the command line does not need to be used again.

You should be familiar with using git from the command line before starting. See the `Command Line` section above.

#### Setup

Navigate to the project on GitLab that you want to clone. At the top of the project page will be buttons labeled `SSH` and `HTTP` beside them will some text which will look similar to: `git@servername:user/repository.git` **note**: Make sure that `SSH` is selected not `HTTP`.

#### Mac Instructions
  1. Add SSH Key to GitLab profile
    - Open Terminal application - `cmd + space` then type `terminal`
    - Check for existing keys - `ls -al ~/.ssh`
      - If not generate SSH Key - `ssh-keygen -t rsa -C "your_email@example.com"`
    - Copy Public Key (_ends in `.pub`_) - `cat ~/.ssh/id_rsa.pub`
    - Add SSH Key to GitLab profile
      - Go to GitLab Profile
      - Click SSH Keys
      - Click Add SSH Key
      - Name and paste in public key.
  2. Download & Install GitHub program -- http://mac.github.com
  3. Create the directory where you want to put the GitLab repository - e.g. `/Users/username/GitLab`
  4. Open the Terminal - `cmd + space` then type `terminal` and press `return`.
  5. Type `git` to check if it's installed, if not click `install` to install Command Line Developer Tools.
  6. Navigate to the directory where you want the repository to go using `cd` e.g. `cd /Users/username/GitLab`.
  7. Then run `git clone git@servername:user/repository.git` which will clone the repository into its own folder in the directory.
  8. In the GitHub app click the `+` icon on the top left, then click `Add`, then click `Choose...` and navigate to the directory you made, and then click `Create & Add Repository`.

You should now be able to see the added repository in the left column under the **Other** section label.

#### Windows Instructions
  1. Add SSH Key to GitLab profile
    - Download and Install PUTTYgen -- http://the.earth.li/~sgtatham/putty/latest/x86/puttygen.exe
    - Run PUTTYgen
    - Click Generate and move mouse in blank area
    - Save Public and Private Key to computer (Private key must be OpenSSL format: Conversions > Export OpenSSH key)
    - Copy Public Key to clipboard
    - Add SSH Key to GitLab profile
      - Go to GitLab Profile
      - Click SSH Keys
      - Click Add SSH Key
      - Name and paste in public key.
  2. Download & Install GitHub program -- http://windows.github.com
  3. Set Private Key into Windows environment on %userprofile%\\.ssh\config file:

```
# GitLab.com server (private key's name must be "id_rsa")
Host gitlab.com
RSAAuthentication yes
IdentityFile X:\where\your\priv\key\located\id_rsa
```

  4. Create new directory for GitLab Repositories - e.g. `C:\Users\username\Documents\GitLab`
  5. Run the `Git Shell` program (_search in the start menu_)
  6. Navigate to GitLab directory using `cd` e.g. `cd C:\Users\username\Documents\GitLab`
  7. Then run `git clone git@gitlab.com:user/repository.git` which will clone the repository into its own folder in the directory.
  8. Open Windows Explorer and navigate to the GitLab folder
  9. Open the GitHub program and drag the repository folder into GitHub program.

You should now be able to see the added repository in the left column under the **Other** section label.

### Best Practices
Git is used differently from Subversion and can be difficult to transition between. Here is a list of some good ways to use Git with some links at the bottom to provide more information.

  1. **Commit Related Changes** - A commit should be a wrapper for related changes. Fixing multiple bugs should produce separate commits. This makes it easier to roll back changes.
  2. **Commit Often** - This keeps your commits small and helps you commit only relevant changes. This also allows you to share your code more quickly with others.
  3. **Don't Commit Half-Done Work** - You should only commit code when it's completed. This doesn’t mean you have to complete a large feature before committing, split the feature’s implementation into logical chunks.
  4. **Test Before You Commit** - Resist the temptation to commit something that you “think” is completed. Test it thoroughly to make sure it really is completed and has no side effects.
  5. **Write Good Commit Messages** - This lets people quickly understand changes without having to read code. Think of a commit like an email, the summary is the subject line, and the description is the main body of the email. The summary should be 50-72 characters in length, and the description should include any bug/issue/request tracking numbers as well as your reasons for making the commit, to provide more detailed information to people trying to understand what has changed. Do not use the same commit messages over again, if the summary has to be the same then explain why in the description.
  6. **Use Branches** - Branches are useful to help you avoid confusing different lines of development. You should use branches extensively in your development workflows: for new features, bug fixes, experiments, or ideas.

See these links for more information:
- https://sethrobertson.github.io/GitBestPractices/
- http://www.git-tower.com/learn/ebook/command-line/appendix/best-practices
- http://nvie.com/posts/a-successful-git-branching-model/
- http://guides.beanstalkapp.com/

## GitLab
GitLab is an open-source 'clone' of GitHub. It's functionality is very similar to that of GitHub, but because it is open-source it means that we can use it on our servers for free.

GitLab allows us to create and modify multiple Git repositories online. It allows admins to manage each repository, the users, and their groups. It has a simple user interface that is very easy to use.

The screenshot below shows all of the controls available on the top bar. Some of the controls are only available to users with certain privileges.

![screenshot 4](resources/screenshot4.png)

| Search | Help | Explore | My Snippets | Admin Area | New Project | Profile Settings | Logout |

### Markdown
Used by GitLab to render any files with the `.md` extension. It is recommended that you create a `README.md` file in each project with information describing the project. This is a good way to create some documentation for a project, such as how to install it.

**Headers**

To create headers put a hash symbol `#` before the text.
```
# H1
## H2
### H3
#### H4
##### H5
###### H6
```

**Emphasis**

```
Italics - *asterisks* or _underscores_

Bold - **asterisks** or __underscores__

Combined emphasis - **asterisks and _underscores_**

Strikethrough uses two tildes, ~~Scratch this~~
```

You can find out more about how GitLab renders Markdown here: https://github.com/gitlabhq/gitlabhq/blob/master/doc/markdown/markdown.md, or you can look at the source code for this project to see how it is done.

### Dashboard
After you log in you will see the Dashboard. The dashboard can be navigated by using the bar on the left, these will show information about all projects and groups that you are members of.

![screenshot 1](resources/screenshot1.png)

While in the Activity tab you can see the activity of each of the projects and groups that you are a member of. This can be filtered by using the selectors at the top of the page:

![screenshot 2](resources/screenshot2.png)

### Project
Once you click on a project you will be navigated to a page which is similar to the main Dashboard, you can see the activity of the project or view the readme. At the side of the screen is a similar menu used to navigate the project. Below is a basic explanation of what most of them do.

![screenshot 3](resources/screenshot3.png)

### Files

When in the files view you can change the branch using the drop down menu at the top of the page. The text to the right of the drop down menu is a breadcrumb trail, which allows you to navigate through different levels in the repository. Beside the breadcrumb trail is a **+** which allows you to add a new file to the repository in the current branch.

![screenshot 5](resources/screenshot5.png)

### Commits

Allows you to see the latest additions to the repository for each branch. Also allows you to compare the differences between commits or branches.

### Issues

Allows users to submit bugs or enhancements. This is where to do lists are stored, as GitLab markdown in project files does not support checkboxes.

### Merge Requests

Allows a user to submit a request to merge two branches together if they do not have permission to merge them. Users can comment and suggest changes, and if they have the permission they can also complete the merge.

### Snippets

If enabled on the project it allows users to paste some code that they need to save.
